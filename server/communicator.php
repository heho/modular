<?php
include_once "authenticator.php";
function handleRequest($request, $config) {

	if (!isset($request["header"]["password"])) {
		throw new Exception("incorrect communicator request format: no password given");
	}

	if (authenticate($request["header"]["password"], $config)) {

		if(!isset($request["header"]["destiny"])) {
			throw new Exception("incorrect communicator request format: no destiny given");
		}

		$body;

		switch ($request["header"]["destiny"]) {
			case "saveTextToFile":

				if (!isset($request["body"]["file"]) ||
					!isset($request["body"]["text"]) ||
					!isset($request["body"]["method"])
				) {
					throw new Exception("Incorrect saveTextToFile format");
				}

				include_once "FileIO.php";
				$body = FileIO::saveTextToFile($request["body"]["file"], $request["body"]["text"], $request["body"]["method"]);

				break;
			default: 
				throw new Exception("Communicator: destiny does not exist");
		}

		$answer = array(
			"head" => array(
				"success" => true
			),
			"body" => $body
		);

		return $answer;
	}
}
?>