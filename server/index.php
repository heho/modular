<?php
//error_reporting(0);
header('Content-Type: application/json');
include_once "config.php";
$answer = array();	
//Handle a request
try {
	if (!isset($_POST["request"]["header"]["service"])) {
		throw new Exception("incorrect request format");
	}

	$service = $_POST["request"]["header"]["service"];

	switch ($service) {
		case "communicator":
			include_once "communicator.php";
			$answer = handleRequest($_POST["request"], $config);
			break;
	}
} catch(Exception $e) {
	$answer = array(
		"header" => array(
			"success" => false,
		),
		"body" => array(
			"errormessage" => $e->getMessage(),
			"errorfile" => $e->getFile(),
			"errorline" => $e->getLine()
		)
	);
}

echo json_encode($answer);