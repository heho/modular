<?php
class FileIO {
	static function saveTextToFile($pathToFile, $text, $method) {
		$mode = "";
		switch ($method) {
			case "overwrite":
				$mode = "w+";
				break;
			case "append":
				$mode = "a";
				break;
			case "prepend":
				$mode = "r+";
				break;
			default:
				throw new Exception("no correct file writing method given");
		}

		$handle = fopen($pathToFile, $mode);
		fwrite($handle, $text);
		fclose($handle);
		return array("message" => "file written");
	}

	static function createFile($pathToFile) {
		
	}	
}

?>