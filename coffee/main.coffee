debug = true

basePath = "./"

modules =
	"config" : "./js/config.js"
	"communicator" : "./js/communicator.js"

vendor = 
	"bootstrap" : "./vendor/bootstrap/js/bootstrap.min.js"

css = 
	"main" : "css/main.css"
	"bootstrap-css" : "./vendor/bootstrap/css/bootstrap.min.css"

loaded = ["jQuery", "jquery"]

console.error "needs jQuery to work properly" if not $

require = (load..., callback) ->
	callback.call() if load.length is 0

	[first, rest...] = load

	next = rest
	next.push callback

	require.apply this, next if first in loaded

	module = null

	if modules[first]?
		module = modular.basePath + modules[first]
	else if vendor[first]?
		module = modular.basePath + vendor[first]
	else if css[first]
		debug and console.log "loading css #{first}"
		head = document.getElementsByTagName('head')[0]
		link = document.createElement 'link'
		link.rel = 'stylesheet'
		link.type = 'text/css'
		link.href = modular.basePath + css[first]
		link.media = 'all'
		head.appendChild link
		loaded.push first
		require.apply this, next
		return
	else
		debug and console.log "coud not load #{first}"
		return

	window.modular.loadingModules++
	debug and console.log "loading #{first}"
	$.getScript(module, (script, textStatus, jqXHR) ->
		#console.log script
		#console.log textStatus 
		#console.log jqXHR.status 
		#console.log "Load was performed." 
		modular.modules.loadingModules--
		require.apply this, next
		loaded.push module
	).fail( (jqxhr, settings, exception) ->
		console.error exception
		require.apply this, next
	)

exportSingle = (path..., object) ->
	ref = window
	depth = 0
	[path..., last] = path
	for part in path
		ref[part] = {} if not ref[part]?
		ref = ref[part]
	ref[last] = {} if not ref[last]?
	ref[last] = object

isExported = (path..., object) ->
	ref = window
	for part in path
		if not ref[part]?
			return false
		ref = ref[part]


exporter = (exports..., callback) ->
	for exp in exports
		if not isExported.apply this, exp 
			exportSingle.apply this, exp
		else
			console.error "Exporter Error: a variable with the name window.#{exp[0]} already exists"
	callback.call()

empty = () ->

exporter ["modular", "modules", modules], 
		["modular", "vendor", vendor], 
		["modular", "css", css],
		["modular", "loadingModules", 0]
		["modular", "debug", debug],
		["modular", "basePath", basePath],
		["require", require], 
		["exporter", exporter],
		["empty", empty], ->