require "jquery", empty

ajaxRequest = (message, destiny, password, callback, fail, always) ->
	json =
		header:
			service: "communicator"
			destiny: destiny
			password: password
		body: message

	request = $.ajax({
		url: modular.basePath + "server/index.php",
		type: "post",
		data: {request: json}
		}, "json");
	request.done (data) -> callback(data)
	request.fail (jqXHR, textStatus, errorThrown) -> fail(jqXHR, textStatus, errorThrown)
	request.always () -> always()

exporter ["communicator", "ajaxRequest", ajaxRequest], ->